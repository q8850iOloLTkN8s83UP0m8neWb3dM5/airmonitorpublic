#!/usr/bin/python3.4
#coding: utf-8

import urllib.request
from configparser import ConfigParser

parser = ConfigParser(allow_no_value=False)
parser.read('/etc/configuration/configuration.data')
wunderground_id=(parser.get('wunderground', 'wunderground_id'))
wunderground_password=(parser.get('wunderground', 'wunderground_password'))

ppm25 = (open('/mnt/ramdisk_ram0/ppm25_0').read())
ppm10 = (open('/mnt/ramdisk_ram0/ppm10_0').read())
tempf = (open('/mnt/ramdisk_ram0/temperature_f_sensor_0').read())
humidity = (open('/mnt/ramdisk_ram0/humidity_sensor_0').read())
pressure_inches = (open('/mnt/ramdisk_ram0/pressure_inches_sensor_0').read())

wunderground = urllib.request.urlopen("https://weatherstation.wunderground.com/weatherstation/updateweatherstation.php?ID=" + str(wunderground_id) + "&PASSWORD=" + str(wunderground_password) +"&dateutc=now&tempf=" + str(tempf) + "&humidity=" + str(humidity) + "&baromin=" + str(pressure_inches) + "&AqPM10=" + str(ppm10) + "&AqPM2.5=" +str(ppm25) + "&action=updateraw", timeout=10)

print(wunderground.read())