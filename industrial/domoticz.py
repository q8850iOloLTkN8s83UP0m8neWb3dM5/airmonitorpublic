#!/usr/bin/python3.4
#coding: utf-8

import json
import urllib.request
from configparser import ConfigParser

parser = ConfigParser()
parser.read('/etc/configuration/configuration.data')

domoticz_ip_address=(parser.get('domoticz', 'domoticz_ip_address'))
domoticz_port=(parser.get('domoticz', 'domoticz_port'))
temperature_sensor_0_idx=(parser.get('domoticz', 'temperature_sensor_0_idx'))
humidity_sensor_0_idx=(parser.get('domoticz', 'humidity_sensor_0_idx'))
pressure_sensor_0_idx=(parser.get('domoticz', 'pressure_sensor_0_idx'))
temp_hum_pressure_sensor_0_idx=(parser.get('domoticz', 'temp_hum_pressure_sensor_0_idx'))

pressure_out_hPa = (open('/mnt/ramdisk_ram0/pressure_hPa_sensor_0').read())
pressure_out_hPa = float(pressure_out_hPa)

temp_out = (open('/mnt/ramdisk_ram0/temperature_sensor_0').read())
temp_out = float(temp_out)

humidity_out = (open('/mnt/ramdisk_ram0/humidity_sensor_0').read())
humidity_out = float(humidity_out)

print(temp_out)
print(pressure_out_hPa)
print(humidity_out)

#urllib.request.urlopen("http://" + str(domoticz_ip_address) + ":" + str(domoticz_port) + "/json.htm?type=command&param=udevice&idx=" + str(pressure_sensor_0_idx) + "&nvalue=0&svalue=" + str(float(pressure_out_hPa)))
#urllib.request.urlopen("http://" + str(domoticz_ip_address) + ":" + str(domoticz_port) + "/json.htm?type=command&param=udevice&idx=" + str(humidity_sensor_0_idx) + "&nvalue=" + str(float(humidity_out)) + "&svalue=HUM_STAT")
#urllib.request.urlopen("http://" + str(domoticz_ip_address) + ":" + str(domoticz_port) + "/json.htm?type=command&param=udevice&idx=" + str(temperature_sensor_0_idx) + "&nvalue=0&svalue=" + str(float(temp_out)))
wyslij=urllib.request.urlopen("http://" + str(domoticz_ip_address) + ":" + str(domoticz_port) + "/json.htm?type=command&param=udevice&idx=" + str(temp_hum_pressure_sensor_0_idx) + "&nvalue=0&svalue=" + str(temp_out) + ";" + str(humidity_out) + ";0;" + str(pressure_out_hPa) + ";0")
print(wyslij.read())
