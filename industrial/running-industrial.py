#!/usr/bin/python3.4
# coding: utf-8
from influxdb import InfluxDBClient
from sds011 import SDS011
import base64
from configparser import ConfigParser
import sys
import urllib3
import time

urllib3.disable_warnings()
parser = ConfigParser()
parser.read('/etc/configuration/configuration.data')
airmonitor_database_ec2_name = (parser.get('airmonitor', 'airmonitor_database_ec2_name'))
airmonitor_database_port = (parser.get('airmonitor', 'airmonitor_database_port'))
airmonitor_database_username = (parser.get('airmonitor', 'airmonitor_database_username'))
airmonitor_database_password = (parser.get('airmonitor', 'airmonitor_database_password'))
airmonitor_database = (parser.get('airmonitor', 'airmonitor_database'))
voivodeship = (parser.get('airmonitor', 'voivodeship'))
city = (parser.get('airmonitor', 'city'))
street = (parser.get('airmonitor', 'street'))
box = (parser.get('airmonitor', 'box'))
placement = (parser.get('airmonitor', 'placement'))
lat = (parser.getfloat('airmonitor', 'lat'))
long = (parser.getfloat('airmonitor', 'long'))
ppm25_sensor_model = (parser.get('airmonitor', 'ppm25_sensor_model'))
ppm10_sensor_model = (parser.get('airmonitor', 'ppm10_sensor_model'))
airmonitor_local_database_username = (parser.get('local', 'airmonitor_database_username'))
airmonitor_local_database_password = (parser.get('local', 'airmonitor_database_password'))
airmonitor_local_host = (parser.get('local', 'airmonitor_database_host'))
sensor_number = (str(sys.argv[1]))

# Create an instance of your sensor
sensor = SDS011('/dev/ttyUSB' + sensor_number)

# Now we have some details about it
print(sensor.device_id)
print(sensor.firmware)
print(sensor.dutycycle)
print(sensor.workstate)
print(sensor.reportmode)
# Set dutycyle to nocycle (permanent)
sensor.reset()
sensor.workstate = SDS011.WorkStates.Measuring
time.sleep(1)

COUNT = 0
pm25_values = []
pm10_values = []
FACTOR = 1.5

while COUNT < 30:
    values = sensor.get_values()
    print("Values measured: PPM10:", values[0], "PPM2.5:", values[1])
    pm25_values.append(values[1])
    pm10_values.append(values[0])
    COUNT += 1
    time.sleep(1)


print("\n\n\n List of PM2,5 values from sensor", pm25_values)
pm25_values_avg = (sum(pm25_values) / len(pm25_values))
print("PM2,5 Average", pm25_values_avg)

for i in pm25_values:
    if pm25_values_avg > i * 1.5:
        pm25_values.remove(max(pm25_values))
        pm25_values_avg = (sum(pm25_values) / len(pm25_values))
        print("Something is not quite right, some PM2,5 value is by 50% than average from last 9 measurements\n")
    elif pm25_values_avg < i * FACTOR:
        print(i, " is OK")
print("Updated list of PM2,5 values", pm25_values)
print("\n\n\n")

print("List of PM10 values from sensor", pm10_values)
pm10_values_avg = (sum(pm10_values) / len(pm10_values))
print("PM10 Average", pm10_values_avg)

for i in pm10_values:
    if pm10_values_avg > i * FACTOR:
        pm10_values.remove(max(pm10_values))
        pm10_values_avg = (sum(pm10_values) / len(pm10_values))
        print("Something is not quite right, some value PM10 value is by 50% than average from last 9 measurements\n")
    elif pm10_values_avg < i * FACTOR:
        print(i, " is OK")
print("Updated list of PM10 values", pm10_values)
print("\n\n\n")

# Values for airmonitor, wunderground and thingspeak
f = open('/mnt/ramdisk_ram0/ppm10_' + sensor_number, 'w')
ppm10 = str(pm10_values_avg)
f.write(ppm10)
f.close()

f = open('/mnt/ramdisk_ram0/ppm25_' + sensor_number, 'w')
ppm25 = str(pm25_values_avg)
f.write(ppm25)
f.close()

json_body_public = [
    {
        "measurement": "ppm25",
        "tags": {
            "voivodeship": voivodeship,
            "city": city,
            "street": street,
            "box": box,
            "placement": placement,
            "lat": lat,
            "long": long,
            "sensor": sensor_number,
            "sensor_model": ppm25_sensor_model
        },
        "fields": {
            "value": float('%.2f' % pm25_values_avg)
        }
    },
    {
        "measurement": "ppm10",
        "tags": {
            "voivodeship": voivodeship,
            "city": city,
            "street": street,
            "box": box,
            "placement": placement,
            "lat": lat,
            "long": long,
            "sensor": sensor_number,
            "sensor_model": ppm10_sensor_model
        },
        "fields": {
            "value": float('%.2f' % pm10_values_avg)
        }
    }
]
client = InfluxDBClient(host='db.airmonitor.pl', port=(base64.b64decode("ODA4Ng==")),
                        username=(base64.b64decode("YWlybW9uaXRvcl9wdWJsaWNfd3JpdGU=")), password=(
        base64.b64decode("amZzZGUwMjh1cGpsZmE5bzh3eWgyMzk4eTA5dUFTREZERkdBR0dERkdFMjM0MWVhYWRm")),
                        database=(base64.b64decode("YWlybW9uaXRvcg==")), ssl=True, verify_ssl=False, timeout=10)
client.write_points(json_body_public)
client_local = InfluxDBClient('192.168.1.242', port=(base64.b64decode("ODA4Ng==")),
                        username=(base64.b64decode("YWlybW9uaXRvcl9wdWJsaWNfd3JpdGU=")), password=(
        base64.b64decode("amZzZGUwMjh1cGpsZmE5bzh3eWgyMzk4eTA5dUFTREZERkdBR0dERkdFMjM0MWVhYWRm")),
                        database=(base64.b64decode("YWlybW9uaXRvcg==")), ssl=True, verify_ssl=False, timeout=10)

client_local.write_points(json_body_public)
print("Values to database: ", json_body_public)

# os.system('/bin/python3.6 /etc/configuration/opensmog_upload.py')

sensor.workstate = SDS011.WorkStates.Sleeping
