#!/usr/bin/python3.4
#coding: utf-8

import urllib.request
from configparser import ConfigParser


parser = ConfigParser(allow_no_value=False)
parser.read('/etc/configuration/configuration.data')
thingspeak_key=(parser.get('thingspeak', 'thingspeak_key'))



ppm25 = (open('/mnt/ramdisk_ram0/ppm25_0').read())
ppm10 = (open('/mnt/ramdisk_ram0/ppm10_0').read())
pressure_out_hPa = (open('/mnt/ramdisk_ram0/pressure_hPa_sensor_0').read())
pressure_out_hPa = float(pressure_out_hPa)
temp_out = (open('/mnt/ramdisk_ram0/temperature_sensor_0').read())
temp_out = float(temp_out)
humidity_out = (open('/mnt/ramdisk_ram0/humidity_sensor_0').read())
humidity_out = float(humidity_out)

thingspeak = urllib.request.urlopen("https://api.thingspeak.com/update.json?api_key=" + str(thingspeak_key) + "&field1=" + str(ppm10) + "&field2=" + str(ppm25) + "&field3=" +str(pressure_out_hPa) + "&field4=" +str(temp_out) + "&field5=" + str(humidity_out), timeout=10)
print(thingspeak.read())
