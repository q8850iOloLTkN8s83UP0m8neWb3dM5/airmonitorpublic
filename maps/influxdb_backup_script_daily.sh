#!/usr/bin/env bash

dump_path="/influxdb_backups/airmonitor/daily"
archive_path="/influxdb_backups_archives/airmonitor/daily"

rm -fr /influxdb_backups*
mkdir -p $archive_path/0/airmonitor
mkdir -p $archive_path/0/_internal
mkdir -p $dump_path/0/airmonitor
mkdir -p $dump_path/0/_internal
s3path_airmonitor_db="airmonitor-backups/influxdb/airmonitor/daily"
s3path_internal_db="airmonitor-backups/influxdb/_internal/daily"

x=7
while [ $x -ge 1 -a $x -le 7 ]
do
  y=$(( $x - 1 ))
  /usr/bin/aws s3 mv s3://$s3path_airmonitor_db/"$y".tar.xz s3://$s3path_airmonitor_db/"$x".tar.xz --storage-class STANDARD
  /usr/bin/aws s3 mv s3://$s3path_internal_db/"$y".tar.xz s3://$s3path_internal_db/"$x".tar.xz --storage-class STANDARD
  x=$(( $x - 1 ))
done


/usr/bin/influxd backup -database airmonitor $dump_path/0/airmonitor
/usr/bin/influxd backup -database _internal $dump_path/0/_internal

tar -cJf $archive_path/0/airmonitor/0.tar.xz $dump_path/0/airmonitor
tar -cJf $archive_path/0/_internal/0.tar.xz $dump_path/0/_internal

/usr/bin/aws s3 cp $archive_path/0/airmonitor/0.tar.xz s3://$s3path_airmonitor_db/ --storage-class STANDARD
/usr/bin/aws s3 cp $archive_path/0/_internal/0.tar.xz s3://$s3path_internal_db/ --storage-class STANDARD

/usr/bin/aws s3 cp /var/lib/grafana/grafana.db s3://airmonitor-application-configurations/grafana/ --storage-class STANDARD
/usr/bin/aws s3 cp /etc/grafana/grafana.ini s3://airmonitor-application-configurations/grafana/ --storage-class STANDARD
/usr/bin/aws s3 cp /etc/yum.repos.d/grafana.repo s3://airmonitor-application-configurations/grafana/ --storage-class STANDARD
/usr/bin/aws s3 cp /etc/influxdb/influxdb.conf s3://airmonitor-application-configurations/influxdb/ --storage-class STANDARD
/usr/bin/aws s3 cp /etc/influxdb/influxdb.pem s3://airmonitor-application-configurations/influxdb/ --storage-class STANDARD
/usr/bin/aws s3 cp /etc/configuration/OTA.sh s3://airmonitor-application-configurations/ --storage-class STANDARD
/usr/bin/aws s3 cp /etc/configuration/OTA1.sh s3://airmonitor-application-configurations/ --storage-class STANDARD
/usr/bin/aws s3 cp /etc/configuration/configuration.data s3://airmonitor-application-configurations/ --storage-class STANDARD
/usr/bin/aws s3 cp /etc/configuration/configuration.file s3://airmonitor-application-configurations/ --storage-class STANDARD
/usr/bin/aws s3 cp /etc/configuration/configuration.file.custom.sensors s3://airmonitor-application-configurations/ --storage-class STANDARD
/usr/bin/aws s3 cp /etc/configuration/influxdb_backup_script_daily.sh s3://airmonitor-application-configurations/ --storage-class STANDARD
/usr/bin/aws s3 cp /etc/configuration/influxdb_backup_script_hourly.sh s3://airmonitor-application-configurations/ --storage-class STANDARD
/usr/bin/aws s3 cp /etc/configuration/loop.py s3://airmonitor-application-configurations/ --storage-class STANDARD
