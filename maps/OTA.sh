#!/usr/bin/env bash

cd /etc/configuration/
/usr/bin/curl -O https://bitbucket.org/q8850iOloLTkN8s83UP0m8neWb3dM5/airmonitorpublic/raw/master/maps/loop.py
/usr/bin/curl -O https://bitbucket.org/q8850iOloLTkN8s83UP0m8neWb3dM5/airmonitorpublic/raw/master/maps/OTA1.sh
/usr/bin/curl -O https://bitbucket.org/q8850iOloLTkN8s83UP0m8neWb3dM5/airmonitorpublic/raw/master/maps/influxdb_backup_script_daily.sh
/usr/bin/curl -O https://bitbucket.org/q8850iOloLTkN8s83UP0m8neWb3dM5/airmonitorpublic/raw/master/maps/influxdb_backup_script_hourly.sh
/bin/chmod +x *.py
/bin/chmod +x *.sh
/etc/configuration/OTA1.sh
